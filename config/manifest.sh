#!/bin/bash
# generate manifest.json for openrefine endpoint

echo "
{
    \"version\": \"2.0\",
    \"mediawiki\": {
      \"name\": \"${WIKIBASE_NAME}\",
      \"root\": \"https://${WIKIBASE_ADDRESS}/wiki/\",
      \"main_page\": \"https://${WIKIBASE_ADDRESS}/wiki/Main_Page\",
      \"api\": \"https://${WIKIBASE_ADDRESS}/w/api.php\"
    },
    \"wikibase\": {
      \"site_iri\": \"https://${WIKIBASE_ADDRESS}/entity/\",
      \"maxlag\": 5,
      \"max_edits_per_minute\": 60,
      \"tag\": \"openrefine-\${version}\",
      \"properties\": {
        \"instance_of\": \"P1\",
        \"subclass_of\": \"P2\"
      },
      \"constraints\": {
        \"property_constraint_pid\": \"P2302\",
        \"exception_to_constraint_pid\": \"P2303\",
        \"constraint_status_pid\": \"P2316\",
        \"mandatory_constraint_qid\": \"Q21502408\",
        \"suggestion_constraint_qid\": \"Q62026391\",
        \"distinct_values_constraint_qid\": \"Q21502410\"
      }
    },
    \"oauth\": {
      \"registration_page\": \"https://${WIKIBASE_ADDRESS}/wiki/Special:OAuthConsumerRegistration/propose\"
    },
    \"entity_types\": {
      \"item\": {
         \"site_iri\": \"https://${WIKIBASE_ADDRESS}/entity/\",
         \"reconciliation_endpoint\": \"http://localhost:8000/\${lang}/api\",
         \"mediawiki_api\": \"https://${WIKIBASE_ADDRESS}/w/api.php\"
      },
      \"property\": {
         \"site_iri\": \"https://${WIKIBASE_ADDRESS}/entity/\",
         \"mediawiki_api\": \"https://${WIKIBASE_ADDRESS}/w/api.php\"
      },
      \"mediainfo\": {
         \"site_iri\": \"https://${WIKIBASE_ADDRESS}/entity/\",
         \"reconciliation_endpoint\": \"${RECONCILE_ADDRESS}/\${lang}/api\"
      }
    },
    \"editgroups\": {
      \"url_schema\": \"([[:toollabs:editgroups-commons/b/OR/\${batch_id}|details]])\"
    }
  }
" > $1

echo "manifest.json processed"
